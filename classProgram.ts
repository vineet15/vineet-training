class Person {
  constructor(
    public name: string,
    public age: number,
    public salary: number,
    public sex: string
  ) {}

  static sort(arr: Person[], field: string, order: string): Person[] {
    if (arr.length == 1) {
      return arr;
    }
    let pivotObj: Person = arr[arr.length - 1];
    let pivot: string | number = pivotObj[field];
    let left: Person[] = [];
    let right: Person[] = [];

    for (const obj of arr.slice(0, arr.length - 1)) {
      let el: string | number = obj[field];
      if (order === "asce") el < pivot ? left.push(obj) : right.push(obj);
      else el > pivot ? left.push(obj) : right.push(obj);
    }
    if (left.length > 0 && right.length > 0) {
      return [
        ...Person.sort(left, field, order),
        pivotObj,
        ...Person.sort(right, field, order),
      ];
    } else if (left.length > 0) {
      return [...Person.sort(left, field, order), pivotObj];
    } else {
      return [pivotObj, ...Person.sort(right, field, order)];
    }
  }
}

let person1: Person = new Person("vineet", 22, 52000, "male");
let person2: Person = new Person("vishva", 21, 22000, "female");
let person3: Person = new Person("amit", 25, 12000, "male");
let person4: Person = new Person("gagan", 23, 25000, "male");

let arr = [person1, person2, person3, person4];

console.log(Person.sort(arr, "age", "desc"));
