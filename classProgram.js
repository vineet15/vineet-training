var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var Person = /** @class */ (function () {
    function Person(name, age, salary, sex) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }
    Person.sort = function (arr, field, order) {
        if (arr.length == 1) {
            return arr;
        }
        var pivotObj = arr[arr.length - 1];
        var pivot = arr[arr.length - 1][field];
        var left = [];
        var right = [];
        for (var _i = 0, _a = arr.slice(0, arr.length - 1); _i < _a.length; _i++) {
            var obj = _a[_i];
            var el = obj[field];
            if (order === "asce")
                el < pivot ? left.push(obj) : right.push(obj);
            else
                el > pivot ? left.push(obj) : right.push(obj);
        }
        if (left.length > 0 && right.length > 0) {
            return __spreadArrays(Person.sort(left, field, order), [
                pivotObj
            ], Person.sort(right, field, order));
        }
        else if (left.length > 0) {
            return __spreadArrays(Person.sort(left, field, order), [pivotObj]);
        }
        else {
            return __spreadArrays([pivotObj], Person.sort(right, field, order));
        }
    };
    return Person;
}());
var person1 = new Person("vineet", 22, 52000, "male");
var person2 = new Person("vishva", 21, 22000, "female");
var person3 = new Person("amit", 25, 12000, "male");
var person4 = new Person("gagan", 23, 25000, "male");
var arr = [person1, person2, person3, person4];
console.log(Person.sort(arr, "age", "de"));
