import { Router, Request, Response, NextFunction } from "express";
const router = Router();

interface RequestWithBody extends Request {
  body: { [key: string]: string | undefined };
}
function requireAuth(req: Request, res: Response, next: NextFunction): void {
  if (req.session && req.session.isLoggedIn) {
    next();
    return;
  }
  res.status(403);
  res.send("Not permitted!!");
}

router.get("/login", (req: Request, res: Response) => {
  res.send(`
    <form method="POST">
      <div>
        <lable>Email</lable>
        <input name="email" type="email" />
      </div>
      <div>
        <lable>Password</lable>
        <input name="password" type="password" />
      </div>
      <button>Submit</button>
    </form>
  `);
});

router.post("/login", (req: RequestWithBody, res: Response) => {
  const { email, password } = req.body;

  if (email && password) {
    //mark user loggedIn
    req.session = { isLoggedIn: true };
    //redirect
    res.redirect("/");
  } else {
    res.send("Invalid Email or Password!");
  }
});

router.get("/", (req: Request, res: Response) => {
  if (req.session && req.session.isLoggedIn) {
    res.send(`
    <div>
      <div>you are logged In</div>
      <a href='/logout'>LogOut</a>
    </div>
    `);
  } else {
    res.send(`
    <div>
      <div>you are not logged In</div>
      <a href='/login'>login</a>
    </div>
    `);
  }
});

router.get("/logout", (req, res) => {
  req.session = undefined;
  res.redirect("/");
});

router.get("/protected", requireAuth, (req, res) => {
  res.send("Welcome to Protected Route,LoggedIN");
});

export { router };
