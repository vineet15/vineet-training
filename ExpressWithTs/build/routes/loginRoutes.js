"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
var express_1 = require("express");
var router = express_1.Router();
exports.router = router;
function requireAuth(req, res, next) {
    if (req.session && req.session.isLoggedIn) {
        next();
        return;
    }
    res.status(403);
    res.send("Not permitted!!");
}
router.get("/login", function (req, res) {
    res.send("\n    <form method=\"POST\">\n      <div>\n        <lable>Email</lable>\n        <input name=\"email\" type=\"email\" />\n      </div>\n      <div>\n        <lable>Password</lable>\n        <input name=\"password\" type=\"password\" />\n      </div>\n      <button>Submit</button>\n    </form>\n  ");
});
router.post("/login", function (req, res) {
    var _a = req.body, email = _a.email, password = _a.password;
    if (email && password) {
        //mark user loggedIn
        req.session = { isLoggedIn: true };
        //redirect
        res.redirect("/");
    }
    else {
        res.send("Invalid Email or Password!");
    }
});
router.get("/", function (req, res) {
    if (req.session && req.session.isLoggedIn) {
        res.send("\n    <div>\n      <div>you are logged In</div>\n      <a href='/logout'>LogOut</a>\n    </div>\n    ");
    }
    else {
        res.send("\n    <div>\n      <div>you are not logged In</div>\n      <a href='/login'>login</a>\n    </div>\n    ");
    }
});
router.get("/logout", function (req, res) {
    req.session = undefined;
    res.redirect("/");
});
router.get("/protected", requireAuth, function (req, res) {
    res.send("Welcome to Protected Route,LoggedIN");
});
