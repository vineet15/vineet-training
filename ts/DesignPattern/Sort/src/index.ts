import { Sorter } from "./Sorter";
import { NumbersCollection } from "./NumbersCollection";
import { CharactersCollection } from "./charatersCollection";

const numbersCollection = new NumbersCollection([10, 2, -3, 493]);
// const sorter = new Sorter(numbersCollection);
const charactersCollection = new CharactersCollection("aasXjd");
charactersCollection.sort();
numbersCollection.sort();

console.log(numbersCollection.data);
console.log(charactersCollection.data);
