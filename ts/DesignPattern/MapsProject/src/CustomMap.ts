export interface Mappable {
  location: {
    long: number;
    lat: number;
  };
}

export class CustomMap {}
