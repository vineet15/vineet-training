import faker from "faker";
import { Mappable } from "./CustomMap";
export class Company implements Mappable {
  companyName: string;
  catchPhrase: string;
  location: {
    long: number;
    lat: number;
  };

  constructor() {
    this.companyName = faker.company.companyName();
    this.catchPhrase = faker.company.catchPhrase();
    this.location = {
      long: parseFloat(faker.address.longitude()),
      lat: parseFloat(faker.address.latitude()),
    };
  }
}
