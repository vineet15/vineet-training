import faker from "faker";
import { Mappable } from "./CustomMap";
export class User implements Mappable {
  firstname: string;
  lastname: string;
  location: {
    long: number;
    lat: number;
  };

  constructor() {
    this.firstname = faker.name.firstName();
    this.lastname = faker.name.lastName();
    this.location = {
      long: parseFloat(faker.address.longitude()),
      lat: parseFloat(faker.address.latitude()),
    };
  }
}
