@classDecortor
class Boat {
  color: string = "red";

  get formattedColor(): string {
    return `This boat color is ${this.color}`;
  }
  @errorDecorator("oops boat was sunk")
  pilot(): void {
    throw new Error();
    // console.log("swiss");
  }

  ocean(@parameterDecorator ocean: string): void {
    console.log(ocean);
  }
}

function classDecortor(constructor: typeof Boat) {
  console.log(constructor);
}
function parameterDecorator(target: Boat, key: string, index: number) {
  console.log(key, index);
}

function propertyDecorator(target: any, key: string, desc: PropertyDescriptor) {
  console.log("target", target);
  console.log("key", key);
}

function errorDecorator(error: string) {
  return function (target: any, key: string, desc: PropertyDescriptor): void {
    const method = desc.value;

    desc.value = function () {
      try {
        method();
      } catch (err) {
        console.log(error);
      }
    };
    // console.log("target", target);
    // console.log(`key,${key}`);
  };
}
new Boat().pilot();
