import "reflect-metadata";

@controller
class Plane {
  color: string = "red";

  @get("/login")
  fly(): void {
    console.log("vrrrrrrr");
  }
}

function get(path: string) {
  return function (target: Plane, key: string) {
    Reflect.defineMetadata("path", path, target, key);
  };
}

function controller(target: typeof Plane) {
  for (let key in target.prototype) {
    const path = Reflect.getMetadata("path", target.prototype, key);
    const middleware = Reflect.getMetadata("middleware", target.prototype, key);

    router.get(path, middleware, target.prototype[key]);
  }
}

// let plane = {
//   color: "red",
// };

// Reflect.defineMetadata("note", "hi there", plane);
// const note = Reflect.getMetadata("note", plane);

// console.log(note);

// Reflect.defineMetadata("note", "hi there", plane, "color");
// const associatedWithProperty = Reflect.getMetadata("note", plane, "color");

// console.log(associatedWithProperty);
