//print a string after a random time

function printString1(string) {
  setTimeout(() => {
    console.log(string);
  }, Math.random() * 100 + 1);
}
// console.log("==================normal");
// printString1("a");
// printString1("b");
// printString1("c");
//prints in radom order

//to avoid it using callbacks
//callback- It is a function which is passed to another function which is called
//           after completion of that function

// console.log("=================callback");
function printString2(string, callback) {
  setTimeout(() => {
    console.log(string);
    callback();
  }, Math.random() * 100 + 1);
}

// printString2("a", () => {
//   printString2("b", () => {
//     printString2("c", () => {});
//   });
// });
//Using we print them in line but it creates callbackHell

// console.log("================promises");

function printString3(string) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log(string);
      resolve();
    }, Math.floor(Math.random() * 100) + 1);
  });
}

// printString3("a")
//   .then(() => printString3("b"))
//   .then(() => printString3("c"));

// Cool async await
async function printAll() {
  await printString1("A");
  await printString1("B");
  await printString1("C");
}
printAll();

//If we want to use previous value
//using callback
// function addString(previous, current, callback) {
//   setTimeout(() => {
//     callback(previous + " " + current);
//   }, Math.floor(Math.random() * 100) + 1);
// }
// function addAll() {
//   addString("", "A", (result) => {
//     addString(result, "B", (result) => {
//       addString(result, "C", (result) => {
//         console.log(result); // Prints out " A B C"
//       });
//     });
//   });
// }
// addAll();

function addString4(previous, current) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(previous + " " + current);
    }, Math.floor(Math.random() * 100) + 1);
  });
}

function addAll() {
  addString4("", "a")
    .then((result) => addString4(result, "b"))
    .then((result) => addString4(result, "c"))
    .then((result) => console.log(result));
}
// addAll();

//using async await

async function addAll() {
  let toPrint = await addString("", "A");
  toPrint = await addString(toPrint, "B");
  toPrint = await addString(toPrint, "C");
  console.log(toPrint); // Prints out " A B C"
}
// addAll();

//Promises Combinators

Promise.all([
  Promise.resolve("success"),
  Promise.resolve("another Success"),
  // Promise.reject("error"),
])
  .then((val) => console.log(val))
  .catch((err) => console.log(err));

Promise.race([
  Promise.resolve("success"),
  Promise.resolve("another Success"),
  // Promise.reject("error"),
])
  .then((val) => console.log(val))
  .catch((err) => console.log(err));

Promise.allSettled([
  Promise.resolve("success"),
  Promise.resolve("another Success"),
  Promise.reject("error"),
])
  .then((val) => console.log(val))
  .catch((err) => console.log(err));

// Promise.any([
//   Promise.resolve("success"),
//   Promise.resolve("another Success"),
//   Promise.reject("error"),
// ])
//   .then((val) => console.log(val))
//   .catch((err) => console.log(err));
