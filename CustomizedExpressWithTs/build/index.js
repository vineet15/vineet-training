"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
// import { router } from "./routes/loginRoutes";
var body_parser_1 = __importDefault(require("body-parser"));
var cookie_session_1 = __importDefault(require("cookie-session"));
var AppRouter_1 = require("./AppRouter");
require("./controllers/LoginController");
require("./controllers/RootController");
var app = express_1.default();
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(cookie_session_1.default({ keys: ["asdfklasjd"] }));
// app.use(router);
app.use(AppRouter_1.AppRouter.getInstance());
//checking route presence
// let route: any,
//   routes: any = [];
// app._router.stack.forEach(function (middleware: any) {
//   if (middleware.route) {
//     // routes registered directly on the app
//     routes.push(middleware.route);
//   } else if (middleware.name === "router") {
//     // router middleware
//     middleware.handle.stack.forEach(function (handler: any) {
//       route = handler.route;
//       console.log(route);
//       route && routes.push(route);
//     });
//   }
// });
// console.log(routes);
app.listen(3000, function () {
    console.log("listening on port 3000");
});
