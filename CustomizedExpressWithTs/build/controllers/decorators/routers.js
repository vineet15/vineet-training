"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.patch = exports.put = exports.del = exports.post = exports.get = exports.routerBinder = void 0;
require("reflect-metadata");
var Methods_1 = require("./Methods");
var MetaDatakeys_1 = require("./MetaDatakeys");
function routerBinder(method) {
    return function (path) {
        return function (target, key, desc) {
            Reflect.defineMetadata(MetaDatakeys_1.MetaDataKeys.path, path, target, key);
            Reflect.defineMetadata(MetaDatakeys_1.MetaDataKeys.method, method, target, key);
        };
    };
}
exports.routerBinder = routerBinder;
exports.get = routerBinder(Methods_1.Methods.get);
exports.post = routerBinder(Methods_1.Methods.post);
exports.del = routerBinder(Methods_1.Methods.del);
exports.put = routerBinder(Methods_1.Methods.put);
exports.patch = routerBinder(Methods_1.Methods.patch);
