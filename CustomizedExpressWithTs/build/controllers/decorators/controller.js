"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.controller = void 0;
require("reflect-metadata");
var AppRouter_1 = require("../../AppRouter");
var MetaDatakeys_1 = require("./MetaDatakeys");
function bodyValidators(keys) {
    return function (req, res, next) {
        if (!req.body) {
            res.status(422).send("Invalid request");
            return;
        }
        for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
            var key = keys_1[_i];
            if (!req.body[key]) {
                res.status(422).send("Invalid request: pass " + key);
                return;
            }
        }
        next();
    };
}
function controller(routePrefix) {
    return function (target) {
        var router = AppRouter_1.AppRouter.getInstance();
        for (var key in target.prototype) {
            var routeHandler = target.prototype[key];
            var path = Reflect.getMetadata(MetaDatakeys_1.MetaDataKeys.path, target.prototype, key);
            //   const method = Reflect.getMetadata("method", target.prototype, key) as Methods;
            var method = Reflect.getMetadata(MetaDatakeys_1.MetaDataKeys.method, target.prototype, key);
            var middlewares = Reflect.getMetadata(MetaDatakeys_1.MetaDataKeys.middleware, target.prototype, key) ||
                [];
            var requireBodyProps = Reflect.getMetadata(MetaDatakeys_1.MetaDataKeys.validator, target.prototype, key) ||
                [];
            var validator = bodyValidators(requireBodyProps);
            if (path) {
                router[method]("" + routePrefix + path, __spreadArrays(middlewares), validator, routeHandler);
            }
        }
    };
}
exports.controller = controller;
