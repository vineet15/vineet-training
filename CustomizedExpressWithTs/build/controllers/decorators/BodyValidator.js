"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bodyValidator = void 0;
require("reflect-metadata");
var MetaDatakeys_1 = require("./MetaDatakeys");
function bodyValidator() {
    var keys = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        keys[_i] = arguments[_i];
    }
    return function (target, key, desc) {
        console.log(keys);
        Reflect.defineMetadata(MetaDatakeys_1.MetaDataKeys.validator, keys, target, key);
    };
}
exports.bodyValidator = bodyValidator;
