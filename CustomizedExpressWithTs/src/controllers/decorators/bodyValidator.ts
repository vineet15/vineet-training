import "reflect-metadata";
import { MetaDataKeys } from "./MetaDatakeys";

export function bodyValidator(...keys: string[]) {
  return function (target: any, key: string, desc: PropertyDescriptor) {
    console.log(keys);
    Reflect.defineMetadata(MetaDataKeys.validator, keys, target, key);
  };
}
