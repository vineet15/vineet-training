import { Request, NextFunction, Response } from "express";
import { controller, get, use } from "./decorators";

function requireAuth(req: Request, res: Response, next: NextFunction): void {
  if (req.session && req.session.isLoggedIn) {
    next();
    return;
  }
  res.status(403);
  res.send("Not permitted!!");
}

@controller("")
class RootController {
  @get("/")
  getRoot(req: Request, res: Response) {
    if (req.session && req.session.isLoggedIn) {
      res.send(`
    <div>
      <div>you are logged In</div>
      <a href='/auth/logout'>LogOut</a>
    </div>
    `);
    } else {
      res.send(`
    <div>
      <div>you are not logged In</div>
      <a href='/auth/login'>login</a>
    </div>
    `);
    }
  }

  @get("/protected")
  @use(requireAuth)
  getProtected(req: Request, res: Response) {
    res.send("Welcome to Protected Route,LoggedIN");
  }
}
