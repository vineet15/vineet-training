import { Request, Response, NextFunction, Router } from "express";
import { get, controller, use, post, bodyValidator } from "./decorators";

@controller("/auth")
class LoginController {
  @get("/login")
  getLogin(req: Request, res: Response): void {
    res.send(`
            <form method="POST">
            <div>
                <lable>Email</lable>
                <input name="email" type="email" />
            </div>
            <div>
                <lable>Password</lable>
                <input name="password" type="password" />
            </div>
            <button>Submit</button>
            </form>
        `);
  }

  @post("/login")
  @bodyValidator("email", "password")
  postLogin(req: Request, res: Response) {
    const { email, password } = req.body;

    if (email && password) {
      //mark user loggedIn
      req.session = { isLoggedIn: true };
      //redirect
      res.redirect("/");
    } else {
      res.send("Invalid Email or Password!");
    }
  }
  @get("/logout")
  getLogout(req: Request, res: Response) {
    req.session = undefined;
    res.redirect("/");
  }
}
