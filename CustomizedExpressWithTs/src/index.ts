import express from "express";
// import { router } from "./routes/loginRoutes";
import bodyParser from "body-parser";
import cookieSession from "cookie-session";
import { AppRouter } from "./AppRouter";

import "./controllers/LoginController";
import "./controllers/RootController";
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieSession({ keys: ["asdfklasjd"] }));
// app.use(router);
app.use(AppRouter.getInstance());

//checking route presence
// let route: any,
//   routes: any = [];
// app._router.stack.forEach(function (middleware: any) {
//   if (middleware.route) {
//     // routes registered directly on the app
//     routes.push(middleware.route);
//   } else if (middleware.name === "router") {
//     // router middleware
//     middleware.handle.stack.forEach(function (handler: any) {
//       route = handler.route;
//       console.log(route);
//       route && routes.push(route);
//     });
//   }
// });
// console.log(routes);

app.listen(3000, () => {
  console.log("listening on port 3000");
});
