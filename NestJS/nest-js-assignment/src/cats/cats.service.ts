import { Injectable } from '@nestjs/common';
import { Cat } from './interfaces/cat.interface';

@Injectable()
export class CatsService {
  private cats: Cat[] = [
    { id: '123', name: 'kitty', age: 2, breed: 'persian' },
    { id: '234', name: 'soldier', age: 4, breed: 'british' },
  ];
  findAll(): Cat[] {
    return this.cats;
  }

  findOne(id: string): Cat {
    return this.cats.find((cat) => cat.id == id);
  }

  update(newCat: Cat, id: string): Cat {
    this.cats = this.cats.map((cat) => {
      if (cat.id == id) return newCat;
      return cat;
    });
    return newCat;
  }

  create(newCat: Cat): Cat {
    this.cats = [...this.cats, newCat];
    return newCat;
  }

  delete(id: string): Cat {
    let deletedCat = this.cats.find((cat) => cat.id == id);
    this.cats = this.cats.filter((cat) => cat.id !== id);
    return deletedCat;
  }
}
