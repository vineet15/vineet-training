import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { CreateCatDto } from './dto/create-cat.dto';
import { Cat } from './interfaces/cat.interface';
import { CatsService } from './cats.service';
import { ValidationPipe } from './validationPipe/validation.pipe';
@Controller('cats')
export class CatsController {
  constructor(private readonly catsService: CatsService) {}
  @Get()
  findAll(): Cat[] {
    return this.catsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id): Cat {
    return this.catsService.findOne(id);
  }

  @Put(':id')
  update(
    @Body(new ValidationPipe()) updateCatDto: CreateCatDto,
    @Param('id', ParseIntPipe) id,
  ): Cat {
    return this.catsService.update(updateCatDto, id);
  }

  @Post()
  create(@Body(new ValidationPipe()) createCatDto: CreateCatDto): Cat {
    return this.catsService.create(createCatDto);
  }

  @Delete(':id')
  delete(@Param('id', ParseIntPipe) id): Cat {
    return this.catsService.delete(id);
  }
}
