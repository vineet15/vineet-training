import { Document } from 'mongoose';
export type ItemDocument = Item & Document;
export interface Item {
  id?: string;
  name: string;
  description?: string;
  qty: number;
}
