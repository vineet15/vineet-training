import { Schema } from 'mongoose';

export const itemSchema = new Schema({
  name: String,
  qty: Number,
  description: String,
});
