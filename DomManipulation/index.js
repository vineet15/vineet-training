function validateForm() {
  const form = document.myform;
  const email = form.email.value;
  const password = form.password.value;
  const gender = form.gender.value;
  const role = form.role.value;

  const emailRegex = /^([a-z\d-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/;
  const passRegex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

  console.log(email, password);
  if (!emailRegex.test(email)) {
    alert("Enter valid email!");
    form.email.focus();
    return false;
  }

  if (!passRegex.test(password)) {
    alert(
      "Enter password with aleast 1 special character, digit and length b/w 6-16"
    );
    form.password.focus();
    return false;
  }
  if (gender === "-1") {
    alert("Enter gender!");
    form.gender.focus();
    return false;
  }
  if (role == "") {
    alert("Select Role");
    return false;
  }
  var checkboxes = document.querySelectorAll('input[type="checkbox"]:checked');
  if (checkboxes.length < 2) {
    alert("Aleast 2 permission should be checked");
    return false;
  }
  const permissions = Array.from(checkboxes).map((item) => item.value);

  form.classList.add("hidden");

  //submit button
  const submitButton = document.querySelector("#submit");
  submitButton.classList.add("hidden");

  //confirm button
  const confirmButton = document.querySelector('input[type="submit"]');
  confirmButton.classList.remove("hidden");

  //detail box
  const detailBox = document.querySelector("#details");
  const para = document.createElement("p");
  para.innerHTML = `Email :${email} <br/> Password: ${password}<br/> Gender:${gender}<br/> Role:${role}<br/> Permission:${permissions} `;
  detailBox.append(para);
  return false;
}
