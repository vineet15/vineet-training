var { Sequelize, DataTypes } = require("sequelize");

var connection = new Sequelize("demoDB", "root", "password", {
  dialect: "mysql",
});

var Artical = connection.define("article", {
  title: { type: DataTypes.STRING },
  body: { type: DataTypes.TEXT },
});

connection.sync();
