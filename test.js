// const regex = /^([a-z\d-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/;
// console.log(regex.test("vineet2983@gmail.com.co"));
// //(\.[a-z]{2,8})?
// const pass = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

// function validate(value, regex) {
//   if (regex.test(value)) {
//     console.log(true);
//   } else {
//     console.log(false);
//   }
// }

// validate("", pass);
//quickSort
function quickSort(arr, order) {
  if (arr.length == 1) {
    return arr;
  }
  let pivot = arr[arr.length - 1];
  let left = [];
  let right = [];

  for (const el of arr.slice(0, arr.length - 1)) {
    if (order === "asce") el < pivot ? left.push(el) : right.push(el);
    else el > pivot ? left.push(el) : right.push(el);
  }
  if (left.length > 0 && right.length > 0) {
    return [...quickSort(left, order), pivot, ...quickSort(right, order)];
  } else if (left.length > 0) {
    return [...quickSort(left, order), pivot];
  } else {
    return [pivot, ...quickSort(right, order)];
  }
}

console.log(quickSort(["vineet", "vikar", "asdf", "ziwefr", "sdfej"], "asce"));
